= Fedora Community Architect Intern Command Center
:license-image: https://licensebuttons.net/l/by-sa/4.0/88x31.png
:license-legal: https://creativecommons.org/licenses/by-sa/4.0/
:license-shield: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

_Lift-off in 3… 2… 1…_
You have now arrived at the Intern Command Center for Fedora Community Architects!
This repository is used for planning Fedora Mentored Projects supported by the Fedora Community Architect and the Fedora Community Operations Team.


[[intro]]
== What happens here?

This repository is used to plan, track progress, and discuss projects that Fedora Community Architect interns are supporting.
At time of writing, we will have two Fedora Community Architect interns in 2024: one employed by Red Hat and one as an Outreachy intern.
Toward the end of May 2024, this repository will be used more as the interns gradually onboard into their internships.


[[howto]]
== How to use this repository

_Coming soon…_


[[legal]]
== Legal

[link={license-legal}]
image::{license-image}[License: Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)]

All content in this repository is shared under the {license-legal}[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license].
